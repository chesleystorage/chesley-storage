Self storage specialists offering cheap container storage and warehouse storage options for business and personal customers at low cost monthly prices. Based Locally to Medway, Sittingbourne & Maidstone, our modern storage facility is clean, secure and offers options in a variety of sizes.

Address: Chesley Farm, Bull Lane, Newington, Kent ME9 7SJ, UK

Phone: +44 1795 844144
